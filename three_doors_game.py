# three doors game
# user needs to correctly guess the door chosen by the computer to win the game
from graphics import *
from button import *
from random import choice

# create animation window
win = GraphWin("three doors", 600, 500, autoflush=False)
win.setCoords(0,0,60,60)
win.setBackground('black')
intro=Text(Point(30,50), "Correctly guess the door chosen by the computer to win the game!\ncreated by Baasil!")
intro.setTextColor('white')
intro.draw(win)

# draw the interface widgets
qb = Button(win, Point(30,10), 5, 3, "Quit")
qb.activate()
qb.setFill('red')
A = Button(win, Point(10,30), 10, 20, "A")
B = Button(win, Point(30,30), 10, 20, "B")
C = Button(win, Point(50,30), 10, 20, "C")
A.setFill('green')
B.setFill('green')
C.setFill('green')
A.activate()
B.activate()
C.activate()
doors = ["A", "B", "C"]
    
# event loop
pt = win.getMouse()
wins=0
loss=0

while not qb.clicked(pt):
    # randomising correct door
    special = choice(doors)
    correct=eval(special)

    if correct.clicked(pt):
        winner = GraphWin("congratulations!", 370, 170)
        winner.setCoords(0,0,20,10)
        Text(Point(10,5), "you won! no big deal tho, you just got lucky\nclick anywhere to close this window").draw(winner)
        try:
            winner.getMouse()
            winner.close()
        except:
            pass
        pt = win.getMouse()
        wins+=1
        
    elif not (A.clicked(pt) or B.clicked(pt) or C.clicked(pt)):
        pt = win.getMouse()

    else:
        loser = GraphWin("loser", 370, 170)
        loser.setCoords(0,0,20,10)
        Text(Point(10,5), "you are the biggest loser I have ever seen in my life!\ncorrect door was {}\nclick anywhere to close this window".format(special)).draw(loser)
        try:
            loser.getMouse()
            loser.close()
        except:
            pass
        pt = win.getMouse()
        loss+=1

if qb.clicked(pt):
    win.close()
    result = GraphWin("result", 370, 170)
    result.setCoords(0,0,20,10)
    Text(Point(10,5), "you won {} games and lost {} games\n click anywhere to close this window.".format(wins, loss)).draw(result)
    try:
        result.getMouse()
        result.close()
    except:
        pass